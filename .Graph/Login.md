# Find Admin url, if you forget admin url
[Magento 2 Beginner Class, Lesson #5: Overview of the Magento Admin @ 0:40](https://youtu.be/aPhjiZAT-Fw?list=PLtaXuX0nEZk9eL59JGE3ny-_GAU-z5X5D&t=38)

file: ~/html/app/etc/env.php

# Change password
https://www.mageplaza.com/kb/how-reset-admin-password-magento-2.html


# Magento Login is in 'bin/setup'
```
--admin-email=john.smith@gmail.com \
--admin-user=john.smith \
--admin-password=password123 \
```

## Disable Two factor Authentication
- [command](https://magento.stackexchange.com/questions/318715/how-to-disable-two-factor-authentication-module-in-magento-2-4)
- [deployment config](https://devdocs.magento.com/guides/v2.4/config-guide/config/config-php.html)
