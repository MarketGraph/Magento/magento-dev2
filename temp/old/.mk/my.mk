# Delete
keyalt:
	# alternate way to setup keys!
	mkdir -p ${HOME}/.composer ${dir}/src
	chmod 666 $(env)/.composer/*.json
	cp -r $(env)/.composer/auth.json ${HOME}/.composer
	cp -r $(env)/.composer/auth.json ${dir}/src

keysetup:
	$(env)/auth.sh

lsconfig:
	composer config -gl

gitcommit:
	cd $(dir)
	git add .
	git commit -am "auto" --quiet
