
me:
	if [ ! -d $(dir) ]; then
		echo 'making my template'
		make init
	else
		echo
	fi

ls:
	docker volume ls
	docker ps -a

onelinesetup:
	make pre
	cd $(dir)
	curl -s https://raw.githubusercontent.com/markshust/docker-magento/master/lib/onelinesetup | bash -s -- magento2.test 2.4.0
